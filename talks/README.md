# Talk materials

Slides, ipython notebooks and data for a talk presented at the Science and Data mini-conference for PyCon Australia (1 Aug 2014) are included.

The slides have been prepared using the html5 presentation engine [Shower](https://github.com/shower/shower).


